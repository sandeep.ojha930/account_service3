FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /build/libs/account-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 8080/tcp
EXPOSE 80/tcp
EXPOSE 8180/tcp
EXPOSE 8181/tcp
EXPOSE 8182/tcp