package com.metrics.account;

import com.metrics.account.domain.Account;
import com.metrics.account.domain.AccountCO;
import com.metrics.account.domain.Saving;
import com.metrics.account.domain.User;
import com.metrics.account.repository.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Collections;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountApplicationTest {
    @Autowired
    WebTestClient webTestClient;
    @Autowired
    AccountRepository accountRepository;

  // @Test
    public void createUssr() {
        User user = new User();
        user.setUsername("sandeep.ojha930");
        user.setPassword("password");
        webTestClient.post().uri("/account").
                contentType(MediaType.APPLICATION_JSON).
                accept(MediaType.APPLICATION_JSON).body(Mono.just(user), User.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody().jsonPath("$.name").isNotEmpty()
                .jsonPath("$.name").isEqualTo("sandeep.ojha930");

    }

    @Test
    public void getAccount(){
        webTestClient.get().uri("/account/{name}", Collections.singletonMap("name","sandeep.ojha930"))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .consumeWith(entityExchangeResult -> Assertions.assertNotEquals(entityExchangeResult,null));
    }
    @Test
    public void updateAccount(){
       Mono<AccountCO> accountCOMono= accountRepository.findByName("sandeep.ojha930").map(account -> {
            AccountCO accountCO = new AccountCO();
            accountCO.setExpenses(account.getExpenses());
            accountCO.setIncomes(account.getIncomes());
            accountCO.setLastSeen(account.getLastSeen());
            accountCO.setName(account.getName());
            Saving saving = account.getSaving();
            saving.setAmount(BigDecimal.valueOf(100));
            accountCO.setSaving(saving);
            accountCO.setNote("Hello Sandeep!!");
            return accountCO;
        });
        webTestClient.put()
                .uri("/account").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(accountCOMono, AccountCO.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.note").isEqualTo("Hello Sandeep!!");
    }


}
