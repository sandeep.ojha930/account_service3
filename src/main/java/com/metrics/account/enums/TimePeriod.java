package com.metrics.account.enums;

public enum TimePeriod {
	YEAR, QUARTER, MONTH, DAY, HOUR
}
