package com.metrics.account.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@Component
public class AccountCO {

    private String name;

    private Date lastSeen;

    private List<Item> incomes;

    private List<Item> expenses;

    @NotNull
    private Saving saving;

    private String note;


    public AccountCO(String name, Date lastSeen, List<Item> incomes, List<Item> expenses, Saving saving, String note) {
        this.name = name;
        this.lastSeen = lastSeen;
        this.incomes = incomes;
        this.expenses = expenses;
        this.saving = saving;
        this.note = note;
    }
}
