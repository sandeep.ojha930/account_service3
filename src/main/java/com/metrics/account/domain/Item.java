package com.metrics.account.domain;

import com.metrics.account.enums.Currency;
import com.metrics.account.enums.TimePeriod;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;

@Data
public class Item {

    @Length(min = 1, max = 20)
    private String title;

    private BigDecimal amount;

    private Currency currency;

    private TimePeriod period;

    private String icon;
}
