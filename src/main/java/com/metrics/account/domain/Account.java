package com.metrics.account.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "accounts")
public class Account {

    @Id
    private ObjectId id;

    @Indexed(unique = true)
    private String name;

    private Date lastSeen;

    private List<Item> incomes;

    private List<Item> expenses;

    @NotNull
    private Saving saving;

    @Length(min = 0, max = 20_000)
    private String note;
}
