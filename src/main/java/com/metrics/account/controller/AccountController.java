package com.metrics.account.controller;

import com.metrics.account.domain.AccountCO;
import com.metrics.account.domain.User;
import com.metrics.account.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/{name}")
    public Mono<ResponseEntity<AccountCO>> getAccountByName(@PathVariable String name) {
        log.info("Fetch the details for " + name);
        return accountService.findByName(name).map(accountCO -> ResponseEntity.ok(accountCO))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping
    public Mono<ResponseEntity<AccountCO>> saveCurrentAccount(@Valid @RequestBody AccountCO account) {
        log.debug("Going to save the account " + account.toString());
        return accountService.saveChanges(account.getName(), account)
                .map(accountCO -> new ResponseEntity<>(accountCO, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public Mono<ResponseEntity<AccountCO>> createNewAccount(@Valid @RequestBody User user) {
        log.info("Create the account for " + user.getUsername());
        return accountService.create(user).map(accountCO -> new ResponseEntity<>(accountCO, HttpStatus.CREATED));
    }
}
