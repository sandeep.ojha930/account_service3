package com.metrics.account.service;

import com.metrics.account.domain.AccountCO;
import com.metrics.account.domain.User;
import reactor.core.publisher.Mono;

public interface AccountService {

    Mono<AccountCO> findByName(String accountName);

    Mono<AccountCO> create(User user);

    Mono<AccountCO> saveChanges(String name, AccountCO update);
}
