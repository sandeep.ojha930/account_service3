package com.metrics.account.service;

import com.metrics.account.domain.Account;
import com.metrics.account.domain.AccountCO;
import com.metrics.account.domain.Saving;
import com.metrics.account.domain.User;
import com.metrics.account.enums.Currency;
import com.metrics.account.repository.AccountRepository;
import com.metrics.account.service.kafka.Sender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class AccountServiceImpl implements AccountService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AccountRepository repository;
    @Autowired
    Sender sender;

    @Override
    public Mono<AccountCO> findByName(String accountName) {
        Assert.hasLength(accountName, "Name must not be empty");
        return repository.findByName(accountName).map(account ->
                new AccountCO(account.getName(), account.getLastSeen(), account.getIncomes(),
                        account.getExpenses(), account.getSaving(), account.getNote()));
    }

    @Override
    public Mono<AccountCO> create(User user) {

        Saving saving = new Saving();
        saving.setAmount(new BigDecimal(0));
        saving.setCurrency(Currency.getDefault());
        saving.setInterest(new BigDecimal(0));
        saving.setDeposit(false);
        saving.setCapitalization(false);

        Account account = new Account();
        account.setName(user.getUsername());
        account.setLastSeen(new Date());
        account.setSaving(saving);
        log.info("new account has been created: " + account.getName());
        Mono<Account> accountMono = repository.save(account);
        sender.send("{\n" +
                "    \"username\": \""+user.getUsername()+"\", \"email\": \""+user.getEmail()+"\"}");
        return accountMono.map(saveAccount ->
                new AccountCO(saveAccount.getName(), saveAccount.getLastSeen(), saveAccount.getIncomes(),
                        saveAccount.getExpenses(), saveAccount.getSaving(), saveAccount.getNote()));
    }

    @Override
    public Mono<AccountCO> saveChanges(String name, AccountCO update) {
        log.debug("account {} changes going to saved", name);
        return repository.findByName(name).flatMap(account -> {
            account.setExpenses(update.getExpenses());
            account.setIncomes(update.getIncomes());
            account.setLastSeen(update.getLastSeen());
            account.setName(update.getName());
            account.setNote(update.getNote());
            account.setSaving(update.getSaving());
            return repository.save(account);
        }).map(updateAccount ->
                new AccountCO(updateAccount.getName(), updateAccount.getLastSeen(), updateAccount.getIncomes(),
                        updateAccount.getExpenses(), updateAccount.getSaving(), updateAccount.getNote()));
    }
}
